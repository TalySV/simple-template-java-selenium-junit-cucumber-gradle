package cucumber.steps;

import extensions.Driver;
import io.cucumber.java.ru.Пусть;

public class MySteps extends Driver {

    @Пусть("Открыта главная")
    public void openMain() {
        Driver.get().navigate().to("http://automationpractice.com/");
    }
}
